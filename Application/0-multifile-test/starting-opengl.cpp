////////////////////////////////////////////////////////////////////////////////
//
// Assignment 1 consists in the following:
//
// - Update your graphics drivers to their latest versions.
// - Download the appropriate libraries GLEW and GLFW for your system.
// - Create a project to compile, link and run the code provided in this
//   section in your favourite programming environment.
// - Verify what OpenGL contexts your computer can support, a minimum of
//   OpenGL 3.3 support is required for this course.
//
// Further suggestions to verify your understanding of the concepts explored:
// - Change the program so display is called at 30 FPS.
//
// (c)2013-22 by Carlos Martinho
//
// INTRODUCES:
// GLFW, GLEW, mglApp.hpp, mglError.hpp
//
////////////////////////////////////////////////////////////////////////////////

#include <iomanip>
#include <iostream>

#include "../../mgl/src/mgl.hpp"

#include "Entity.h"

////////////////////////////////////////////////////////////////////////// MYAPP

class MyApp : public mgl::App {
public:
  void initCallback(GLFWwindow *win) override;
  void displayCallback(GLFWwindow *win, double elapsed) override;
  void windowCloseCallback(GLFWwindow *win) override;
  void windowSizeCallback(GLFWwindow *win, int width, int height) override;
  void keyCallback(GLFWwindow *win, int key, int scancode, int action,
                   int mods) override;
  void cursorCallback(GLFWwindow *win, double xpos, double ypos) override;
  void mouseButtonCallback(GLFWwindow *win, int button, int action,
                           int mods) override;
  void scrollCallback(GLFWwindow *win, double xoffset, double yoffset) override;
  void joystickCallback(int jid, int event) override;

private:
  void updateFPS(GLFWwindow *win, double ellapsed_sec);
};

/////////////////////////////////////////////////////////////////////////// INIT

void MyApp::initCallback(GLFWwindow *win) {
  std::cout << "Initializing..." << std::endl;
}

////////////////////////////////////////////////////////////////////// CALLBACKS

void MyApp::windowCloseCallback(GLFWwindow *win) {
  std::cout << "closing..." << std::endl;
}

void MyApp::windowSizeCallback(GLFWwindow *win, int width, int height) {
  std::cout << "size: " << width << " " << height << std::endl;
  glViewport(0, 0, width, height);
}

void MyApp::keyCallback(GLFWwindow *win, int key, int scancode, int action,
                        int mods) {
  std::cout << "key: " << key << " " << scancode << " " << action << " " << mods
            << std::endl;
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(win, GLFW_TRUE);
    windowCloseCallback(win);
  }
}

void MyApp::cursorCallback(GLFWwindow *win, double xpos, double ypos) {
  std::cout << "mouse: " << xpos << " " << ypos << std::endl;
}

void MyApp::mouseButtonCallback(GLFWwindow *win, int button, int action,
                                int mods) {
  std::cout << "button: " << button << " " << action << " " << mods
            << std::endl;
}

void MyApp::scrollCallback(GLFWwindow *win, double xoffset, double yoffset) {
  std::cout << "scroll: " << xoffset << " " << yoffset << std::endl;
}

void MyApp::joystickCallback(int jid, int event) {
  std::cout << "joystick: " << jid << " " << event << std::endl;
}

//////////////////////////////////////////////////////////////////////////// RUN

void MyApp::displayCallback(GLFWwindow *win, double elapsed_sec) {
  updateFPS(win, elapsed_sec);
}

void MyApp::updateFPS(GLFWwindow *win, double elapsed_sec) {
  static unsigned int acc_frames = 0;
  static double acc_time = 0.0;
  const double UPDATE_TIME = 1.0;

  ++acc_frames;
  acc_time += elapsed_sec;
  if (acc_time > UPDATE_TIME) {
    std::ostringstream oss;
    double fps = acc_frames / acc_time;
    oss << std::fixed << std::setw(5) << std::setprecision(1) << fps << " FPS";
    glfwSetWindowTitle(win, oss.str().c_str());

    acc_frames = 0;
    acc_time = 0.0;
  }
}

/////////////////////////////////////////////////////////////////////////// MAIN

int main(int argc, char *argv[]) {
  int gl_major = 4, gl_minor = 3;
  int is_fullscreen = 0;
  int is_vsync = 1;

  Entity entity;
  entity.name = std::string("a entity");
  
  std::cout << entity.name << std::endl;

  mgl::Engine &engine = mgl::Engine::getInstance();
  engine.setApp(new MyApp());
  engine.setOpenGL(gl_major, gl_minor);
  engine.setWindow(800, 600, "MGL Window", is_fullscreen, is_vsync);
  engine.init();
  engine.run();
  exit(EXIT_SUCCESS);
}

//////////////////////////////////////////////////////////////////////////// END
