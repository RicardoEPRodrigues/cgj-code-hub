////////////////////////////////////////////////////////////////////////////////
//
// An introduction to using quaternions to rotate in 3D.
//
// (c) 2013-22 by Carlos Martinho
//
// INTRODUCES:
// ROTATION PROBLEMS, QUATERNIONS
//
////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>

////////////////////////////////////////////////////////////////////////////////

const float THRESHOLD = (float)1.0e-5;
#define HEADER(X) std::cout << std::endl << (X) << std::endl << std::endl;

void qtest1() {
  HEADER("TEST 1 : Rotation of 90 degrees about the y-axis")

  glm::vec3 axis = {0.0f, 1.0f, 0.0f};
  glm::quat q = glm::angleAxis(glm::radians(90.0f), axis);
  std::cout << "    q=" << glm::to_string(q) << std::endl;

  glm::quat qi = {0.0f, 7.0f, 0.0f, 0.0f};
  std::cout << "   vi=" << glm::to_string(qi) << std::endl;

  glm::quat qe = {0.0f, 0.0f, 0.0f, -7.0f};
  std::cout << "   qe=" << glm::to_string(qe) << std::endl;

  glm::quat qinv = glm::inverse(q);
  std::cout << " qinv=" << glm::to_string(qinv) << std::endl;

  glm::quat qf1 = q * qi * qinv;
  std::cout << "  qf1=" << glm::to_string(qf1) << std::endl;

  assert(glm::all(glm::equal(qf1, qe, THRESHOLD)));

  glm::quat qconj = glm::conjugate(q);
  std::cout << "qconj=" << glm::to_string(qconj) << std::endl;

  glm::quat qf2 = q * qi * qconj;
  std::cout << "  qf2=" << glm::to_string(qf2) << std::endl;

  assert(glm::all(glm::equal(qf2, qe, THRESHOLD)));
}

void qtest2() {
  HEADER("TEST 2 : Rotation of 90 degrees about the y-axis with matrix")

  glm::vec3 axis = {0.0f, 1.0f, 0.0f};
  glm::quat q = glm::angleAxis(glm::radians(90.0f), axis);
  std::cout << "    q=" << glm::to_string(q) << std::endl;

  glm::vec4 vi = {7.0f, 0.0f, 0.0f, 1.0f};
  std::cout << "   vi=" << glm::to_string(vi) << std::endl;

  glm::vec4 ve = {0.0f, 0.0f, -7.0f, 1.0f};
  std::cout << "   ve=" << glm::to_string(ve) << std::endl;

  glm::mat4 m = glm::toMat4(q);
  std::cout << "    m=" << glm::to_string(m) << std::endl;

  glm::vec4 vf = m * vi;
  std::cout << "   vf=" << glm::to_string(vf) << std::endl;

  assert(glm::all(glm::equal(vf, ve, THRESHOLD)));
}

void qtest3() {
  HEADER("TEST 3 : Yaw 900 = Roll 180 + Pitch 180")

  glm::vec3 axis_x = {1.0f, 0.0f, 0.0f};
  glm::vec3 axis_y = {0.0f, 1.0f, 0.0f};
  glm::vec3 axis_z = {0.0f, 0.0f, 1.0f};

  glm::quat qyaw900 = glm::angleAxis(glm::radians(900.0f), axis_y);
  std::cout << "  qyaw900=" << glm::to_string(qyaw900) << std::endl;

  glm::quat qroll180 = glm::angleAxis(glm::radians(180.0f), axis_x);
  std::cout << "  qroll180=" << glm::to_string(qroll180) << std::endl;
  glm::quat qpitch180 = glm::angleAxis(glm::radians(180.0f), axis_z);
  std::cout << " qpitch180=" << glm::to_string(qpitch180) << std::endl;
  glm::quat qrp = qpitch180 * qroll180;
  glm::quat qpr = qroll180 * qpitch180;

  glm::quat qi = {0.0f, 1.0f, 0.0f, 0.0f}; // x-axis
  std::cout << "        qi=" << glm::to_string(qi) << std::endl;
  glm::quat qe = {0.0f, -1.0f, 0.0f, 0.0f};
  std::cout << "        qe=" << glm::to_string(qe) << std::endl;

  glm::quat qfy = qyaw900 * qi * glm::inverse(qyaw900);
  std::cout << "       qfy=" << glm::to_string(qfy) << std::endl;
  assert(glm::all(glm::equal(qfy, qe, THRESHOLD)));

  glm::quat qfrp = qrp * qi * glm::inverse(qrp);
  std::cout << "      qfrp=" << glm::to_string(qfrp) << std::endl;
  assert(glm::all(glm::equal(qfrp, qe, THRESHOLD)));

  glm::quat qfpr = qpr * qi * glm::inverse(qpr);
  std::cout << "      qfpr=" << glm::to_string(qfpr) << std::endl;
  assert(glm::all(glm::equal(qfpr, qe, THRESHOLD)));
}

void qtest4() {
  HEADER("TEST 4: Q <-> (angle, axis)")

  float theta_i = glm::radians(45.0f);
  glm::vec3 axis_i = {0.0f, 1.0f, 0.0f};
  glm::quat q = glm::angleAxis(theta_i, axis_i);
  std::cout << "theta_i=" << theta_i << " ";
  std::cout << "axis_i=" << glm::to_string(axis_i) << std::endl;

  float theta_f = glm::angle(q);
  glm::vec3 axis_f = glm::axis(q);
  std::cout << "theta_f=" << theta_f << " ";
  std::cout << "axis_f=" << glm::to_string(axis_f) << std::endl;

  assert((fabs(theta_i - theta_f) < THRESHOLD) &&
         glm::all(glm::equal(axis_i, axis_i, THRESHOLD)));
}

void qtest5() {
  HEADER("TEST 5: LERP")

  glm::vec3 axis = {0.0f, 1.0f, 0.0f};
  glm::quat q0 = glm::angleAxis(glm::radians(0.0f), axis);
  std::cout << "     q0=" << glm::to_string(q0) << std::endl;
  glm::quat q1 = glm::angleAxis(glm::radians(90.0f), axis);
  std::cout << "     q1=" << glm::to_string(q1) << std::endl;
  glm::quat qe = glm::angleAxis(glm::radians(30.0f), axis);
  std::cout << "     qe=" << glm::to_string(qe) << std::endl;

  glm::quat qLerp0 = glm::lerp(q0, q1, 0.0f);
  std::cout << " qLerp0=" << glm::to_string(qLerp0) << std::endl;
  assert(glm::all(glm::equal(qLerp0, q0, THRESHOLD)));

  glm::quat qLerp1 = glm::lerp(q0, q1, 1.0f);
  std::cout << " qLerp1=" << glm::to_string(qLerp1) << std::endl;
  assert(glm::all(glm::equal(qLerp1, q1, THRESHOLD)));

  glm::quat qLerp = glm::lerp(q0, q1, 1 / 3.0f);
  std::cout << "  qLerp=" << glm::to_string(qLerp) << std::endl;
  assert(glm::any(glm::notEqual(qLerp, qe, THRESHOLD)));
}

void qtest6() {
  HEADER("TEST 6: SLERP")

  glm::vec3 axis = {0.0f, 1.0f, 0.0f};
  glm::quat q0 = glm::angleAxis(glm::radians(0.0f), axis);
  std::cout << "      q0=" << glm::to_string(q0) << std::endl;
  glm::quat q1 = glm::angleAxis(glm::radians(90.0f), axis);
  std::cout << "      q1=" << glm::to_string(q1) << std::endl;
  glm::quat qe = glm::angleAxis(glm::radians(30.0f), axis);
  std::cout << "      qe=" << glm::to_string(qe) << std::endl;

  glm::quat qSlerp0 = glm::slerp(q0, q1, 0.0f);
  std::cout << " qSlerp0=" << glm::to_string(qSlerp0) << std::endl;
  assert(glm::all(glm::equal(qSlerp0, q0, THRESHOLD)));

  glm::quat qSlerp1 = glm::slerp(q0, q1, 1.0f);
  std::cout << " qSlerp1=" << glm::to_string(qSlerp1) << std::endl;
  assert(glm::all(glm::equal(qSlerp1, q1, THRESHOLD)));

  glm::quat qSlerp = glm::slerp(q0, q1, 1 / 3.0f);
  std::cout << "  qSlerp=" << glm::to_string(qSlerp) << std::endl;
  assert(glm::all(glm::equal(qSlerp, qe, THRESHOLD)));
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
  qtest1();
  qtest2();
  qtest3();
  qtest4();
  qtest5();
  qtest6();
  exit(EXIT_SUCCESS);
}

////////////////////////////////////////////////////////////////////////////////
