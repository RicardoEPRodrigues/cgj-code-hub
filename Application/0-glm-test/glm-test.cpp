////////////////////////////////////////////////////////////////////////////////
//
// OpenGL Math (GLM) lab exercices.
//
// (c) 2016-2022 by Carlos Martinho
//
// INTRODUCES:
// GLM
//
////////////////////////////////////////////////////////////////////////////////

#include <ctime>
#include <glm/ext/matrix_relational.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include <stdexcept>
#include <vector>

#define PRINT(X) std::cout << (X) << std::endl << std::endl;
const float THRESHOLD = (float)1.0e-5;

////////////////////////////////////////////////////////////////////////////////

void test_vec3a() {
  PRINT("Rodrigues Rotation Formula (vector form)")
  std::vector<glm::vec3> vectors = {glm::vec3{2.0f, 0.0f, 0.0f},
                                    glm::vec3{0.0f, 3.0f, 0.0f},
                                    glm::vec3{0.0f, 0.0f, 4.0f}};
  float angle_deg = 90.0f;
  float angle = glm::radians(angle_deg);

  std::vector<glm::vec3> expected = {
      glm::vec3(2.0f, 0.0f, 0.0f),  glm::vec3(0.0f, 0.0f, 3.0f),
      glm::vec3(0.0f, -4.0f, 0.0f), glm::vec3(0.0f, 0.0f, -2.0f),
      glm::vec3(0.0f, 3.0f, 0.0f),  glm::vec3(4.0f, 0.0f, 0.0f),
      glm::vec3(0.0f, 2.0f, 0.0f),  glm::vec3(-3.0f, 0.0f, 0.0f),
      glm::vec3(0.0f, 0.0f, 4.0f)};
  int idx = 0;
  int err = 0;

  for (glm::vec3 &a : vectors) {
    for (glm::vec3 &v : vectors) {
      glm::vec3 axis = normalize(a);
      glm::vec3 vrot = cosf(angle) * v + sinf(angle) * cross(axis, v) +
                       axis * (glm::dot(axis, v)) * (1 - cosf(angle));
      if (glm::any(glm::notEqual(vrot, expected[idx++], THRESHOLD))) {
        std::cout << "ERROR FOUND:" << std::endl;
        std::cout << "v=" << glm::to_string(v)
                  << " axis=" << glm::to_string(axis) << " angle=" << angle_deg
                  << std::endl;
        std::cout << "  vrot=" << glm::to_string(vrot) << std::endl
                  << std::endl;
        err++;
      }
    }
  }
  std::cout << err << " error(s) found." << std::endl << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

inline float random_float_1() {
  return static_cast<float>(rand()) / (static_cast<float>(RAND_MAX) / 2.0f) -
         1.0f;
}
inline glm::vec3 random_unit_vec3() {
  glm::vec3 v = {random_float_1(), random_float_1(), random_float_1()};
  return normalize(v);
}
void test_vec3b() {
  PRINT("Vector Triple Product")

  srand(static_cast<unsigned>(time(0)));
  int err = 0;

  const int max_iterations = 10;
  for (int it = 0; it < max_iterations; it++) {
    glm::vec3 i = it ? random_unit_vec3() : glm::vec3{};
    // null vector test on first iteration
    glm::vec3 j = random_unit_vec3();
    glm::vec3 k = random_unit_vec3();
    glm::vec3 v1 = glm::cross(i, glm::cross(j, k));
    glm::vec3 v2 = j * (glm::dot(i, k)) - k * (glm::dot(i, j));

    if (glm::any(glm::notEqual(v1, v2, THRESHOLD))) {
      std::cout << "ERROR FOUND" << std::endl;
      std::cout << "i=" << glm::to_string(i) << " j=" << glm::to_string(j)
                << " k=" << glm::to_string(k) << std::endl;
      std::cout << "=> " << glm::to_string(v1) << " == " << glm::to_string(v2)
                << std::endl
                << std::endl;
      err++;
    }
  }
  std::cout << err << " error(s) found." << std::endl << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

void test_vec3c() {
  PRINT("Camera Coordinate Frame from View and Up vectors")

  std::vector<glm::vec3> vectors = {glm::vec3{1.0f, 0.0f, 0.0f},
                                    glm::vec3{0.0f, 2.0f, 0.0f},
                                    glm::vec3{0.0f, 0.0f, 3.0f}};

  for (glm::vec3 &view : vectors) {
    for (glm::vec3 &up : vectors) {
      try {
        std::cout << "view=" << glm::to_string(view)
                  << " up=" << glm::to_string(up) << std::endl;
        glm::vec3 v = normalize(view);
        glm::vec3 w = normalize(glm::cross(up, v));
        glm::vec3 u = cross(v, w);
        std::cout << "u=" << glm::to_string(u) << " v=" << glm::to_string(v)
                  << " w=" << glm::to_string(w) << std::endl;
      } catch (std::exception &e) {
        std::cout << e.what() << std::endl;
      }
      std::cout << std::endl;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

inline glm::mat3 dual_mat3(glm::vec3 &v) {
  float a[9] = {0.0f, v.z, -v.y, -v.z, 0.0f, v.x, v.y, -v.x, 0.0f};
  return glm::make_mat3(a);
}
void test_mat3a() {
  PRINT("Rodrigues Rotation Formula (matrix form)")

  std::vector<glm::vec3> vectors = {glm::vec3{2.0f, 0.0f, 0.0f},
                                    glm::vec3{0.0f, 3.0f, 0.0f},
                                    glm::vec3{0.0f, 0.0f, 4.0f}};
  float angle_deg = 90.0f;
  float angle = glm::radians(angle_deg);

  std::vector<glm::vec3> expected = {
      glm::vec3(2.0f, 0.0f, 0.0f),  glm::vec3(0.0f, 0.0f, 3.0f),
      glm::vec3(0.0f, -4.0f, 0.0f), glm::vec3(0.0f, 0.0f, -2.0f),
      glm::vec3(0.0f, 3.0f, 0.0f),  glm::vec3(4.0f, 0.0f, 0.0f),
      glm::vec3(0.0f, 2.0f, 0.0f),  glm::vec3(-3.0f, 0.0f, 0.0f),
      glm::vec3(0.0f, 0.0f, 4.0f)};
  int idx = 0;
  int err = 0;

  glm::mat3 I = glm::mat3{1.0f};
  for (glm::vec3 &a : vectors) {
    glm::vec3 axis = normalize(a);
    glm::mat3 A = dual_mat3(axis);
    glm::mat3 A2 = A * A;
    glm::mat3 R = I + sinf(angle) * A + (1 - cosf(angle)) * A2;
    for (glm::vec3 &v : vectors) {
      glm::vec3 vrot = R * v;
      if (glm::any(glm::notEqual(vrot, expected[idx++], THRESHOLD))) {
        std::cout << "ERROR FOUND" << std::endl;
        std::cout << "v=" << glm::to_string(v)
                  << " axis=" << glm::to_string(axis) << " angle=" << angle_deg
                  << std::endl;
        std::cout << "  vrot=" << glm::to_string(vrot) << std::endl
                  << std::endl;
        err++;
      }
    }
  }
  std::cout << err << " error(s) found." << std::endl << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

void test_mat3b() {
  PRINT("Model and Normal matrices")

  glm::mat4 t = glm::translate(glm::vec3{1.0f, 0.0f, 0.0f});
  glm::mat4 r = glm::rotate(glm::radians(90.0f), glm::vec3{0.0f, 1.0f, 0.0f});
  glm::mat4 s = glm::scale(glm::vec3{2.0f, 2.0f, 2.0f});
  std::cout << "T = " << std::endl << glm::to_string(t) << std::endl;
  std::cout << "R = " << std::endl << glm::to_string(r) << std::endl;
  std::cout << "S =" << std::endl << glm::to_string(s) << std::endl;
  glm::mat4 model_matrix = t * r * s;
  std::cout << "ModelMatrix =" << std::endl
            << glm::to_string(model_matrix) << std::endl;
  glm::mat3 model_matrix_3x3 = glm::mat3(model_matrix);
  std::cout << "ModelMatrix (3x3) =" << std::endl
            << glm::to_string(model_matrix_3x3) << std::endl;
  glm::mat3 normal_matrix = glm::transpose(glm::inverse(model_matrix_3x3));
  std::cout << "NormalMatrix =" << std::endl
            << glm::to_string(normal_matrix) << std::endl;
  glm::vec4 vi = {0.5f, 0.0f, 0.0f, 1.0f};
  glm::vec3 ni = {0.0f, 1.0f, 0.0f};
  std::cout << "vi=" << glm::to_string(vi) << " ni=" << glm::to_string(ni)
            << std::endl;
  glm::vec4 vf = model_matrix * vi;
  glm::vec3 nf = normal_matrix * ni;
  std::cout << "vf=" << glm::to_string(vf) << " nf=" << glm::to_string(nf)
            << std::endl;
  std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

inline float random_int_9() { return static_cast<float>(rand() % 19 - 9); }
inline glm::mat3 random_invertible_matrix_3x3() {
  glm::mat3 m;
  do {
    m = {random_int_9(), random_int_9(), random_int_9(),
         random_int_9(), random_int_9(), random_int_9(),
         random_int_9(), random_int_9(), random_int_9()};
  } while (glm::determinant(m) == 0);
  return m;
}
inline bool not_equal_mat3(glm::mat3 m1, glm::mat3 m2) {
  glm::vec3 diff = glm::notEqual(m1, m2, THRESHOLD);
  return (diff.x || diff.y || diff.z);
}
void test_mat3c() {
  PRINT("Inverse and Transpose Matrices")
  srand(static_cast<unsigned>(time(0)));
  int err = 0;

  const int max_iterations = 10;
  for (int it = 0; it < max_iterations; it++) {
    glm::mat3 m1 = random_invertible_matrix_3x3();
    glm::mat3 m2 = random_invertible_matrix_3x3();

    try {
      glm::mat3 m_t1 = glm::transpose(m1 * m2);
      glm::mat3 m_t2 = glm::transpose(m2) * glm::transpose(m1);

      if (not_equal_mat3(m_t1, m_t2)) {
        std::cout << "ERROR FOUND :: TRANSPOSE" << std::endl;
        std::cout << "transpose(M1*M2) =" << std::endl
                  << glm::to_string(m_t1) << std::endl;
        std::cout << "transpose(M2)*transpose(M1)= " << std::endl
                  << glm::to_string(m_t2) << std::endl
                  << std::endl;
        err++;
      }

      glm::mat3 m_inv1 = glm::inverse(m1 * m2);
      glm::mat3 m_inv2 = glm::inverse(m2) * glm::inverse(m1);

      if (not_equal_mat3(m_inv1, m_inv2)) {
        std::cout << "ERROR FOUND :: INVERSE" << std::endl;
        std::cout << "inverse(M1*M2) =" << std::endl
                  << glm::to_string(m_inv1) << std::endl;
        std::cout << "inverse(M2)*inverse(M1) =" << std::endl
                  << glm::to_string(m_inv2) << std::endl
                  << std::endl;
        err++;
      }
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl << std::endl;
    }
  }
  std::cout << err << " error(s) found." << std::endl << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

void test_mat3d() {
  PRINT("Combined Inverse and Transpose Matrices")
  srand(static_cast<unsigned>(time(0)));
  int err = 0;

  const int max_iterations = 10;
  for (int it = 0; it < max_iterations; it++) {
    glm::mat3 mA = random_invertible_matrix_3x3();
    glm::mat3 mB = random_invertible_matrix_3x3();
    glm::mat3 mC = random_invertible_matrix_3x3();
    glm::mat3 mD = random_invertible_matrix_3x3();

    try {
      glm::mat3 m_1 =
          glm::transpose(glm::inverse(mA * mB) * glm::inverse(mC * mD));
      glm::mat3 m_2 = glm::transpose(glm::inverse(mD) * glm::inverse(mC)) *
                      glm::transpose(glm::inverse(mB) * glm::inverse(mA));

      if (not_equal_mat3(m_1, m_2)) {
        std::cout << "ERROR FOUND" << std::endl;
        std::cout << "(inv(AB)*inv(CD))T =" << std::endl
                  << glm::to_string(m_1) << std::endl;
        std::cout << "(inv(D)*inv(C))T*(inv(B)*inv(A))T =" << std::endl
                  << glm::to_string(m_2) << std::endl
                  << std::endl;
        err++;
      }
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl << std::endl;
    }
  }
  std::cout << err << " error(s) found." << std::endl << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

inline float random_float() {
  return static_cast<float>(rand() % 19 - 9);
}

int main(int argc, char *argv[]) {
  srand (time(NULL));

  glm::vec3 a = {random_float(), random_float(), random_float()};
  glm::vec3 b = {random_float(), random_float(), random_float()};

  float dist = glm::distance(a, b);

  std::cout << glm::to_string(a) << " | " << glm::to_string(b) << " | " << dist << std::endl;

  exit(EXIT_SUCCESS);
}

////////////////////////////////////////////////////////////////////////////////
// clang-format -style=llvm -dump-config > .clang-format
