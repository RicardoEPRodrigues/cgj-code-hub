# CGJ Classes code hub.

Just some bits of code to play around with.

If you make use of Pop_OS! 22.04 (this might be applicable to other Ubuntu distros), here are the dependencies needed to compile and run the software:

```bash
# C++ Dependencies
sudo apt install libc++-dev libstdc++-12-dev

# GLM Dependencies
sudo apt install libglm-dev

# OpenGL/GLEW/GLFW Dependencies
sudo apt install libglew-dev libglfw3-dev libassimp-dev
```