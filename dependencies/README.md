This project requires a couple of dependencies.

Please install the dependencies here with the following structure:

* **GLM 0.9.9.8** - make sure the folder is named `glm` and that it contains another folder named `glm` inside with the code.
    
    https://github.com/g-truc/glm/releases/tag/0.9.9.8

* **GLFW 3.3.8** - make sure the folder is named `glfw` and that it contains another folder named `include` inside with the code (along with the libraries).

    https://www.glfw.org/download.html (download the pre-compiled binaries for Windows x64)

* **GLEW 2.1.0** - make sure the folder is named `glew` and that it contains another folder named `include` inside with the code (along with the libraries).

    https://glew.sourceforge.net/ (download the pre-compiled binaries for Windows x64)