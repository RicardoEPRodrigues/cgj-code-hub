MAKE := make

all: build

build:
	$(MAKE) -C ./mgl
	$(MAKE) -C ./Application

run: build
	$(MAKE) -C ./Application/0-multifile-test run

clean:
	$(MAKE) -C ./mgl clean
	$(MAKE) -C ./Application clean