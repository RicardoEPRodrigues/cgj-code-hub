#ifndef MGL_HPP
#define MGL_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "mglConventions.hpp"
#include "mglShader.hpp"

#include "mglApp.hpp"
#include "mglError.hpp"

#endif /* MGL_HPP */
